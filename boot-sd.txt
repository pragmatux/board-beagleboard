#
# NOTE: This file is used only when booting a first-stage image
# from SD. The boot.scr produced from this file is overwritten
# by update-bootscr during postinst.
#
# IMPORTANT: This file must produce an SD-bootable boot.scr.
# See boot.txt.in for all other use cases.
#

# TODO: replace this with an auto-generated uname.
setenv uname 5.10.65-ti-r24

setenv dtb am335x-boneblack-uboot-univ.dtb

setenv BOOTDEV 0
setenv BOOTPART 1
setenv bootpart ${BOOTDEV}:${BOOTPART}

part uuid mmc ${bootpart} uuid
echo $board_name

if test $board_name = BBBW; then
    setenv dtb am335x-boneblack-wireless.dtb
fi
if test $board_name = AM335BONE; then
    setenv dtb am335x-bone.dtb
fi

load mmc ${bootpart} ${fdtaddr} /boot/dtbs/${uname}/${dtb}

setenv fdt_buffer 0x60000

load mmc ${bootpart} ${rdaddr} /lib/firmware/BB-BONE-eMMC0-01-00A0.dtbo
fdt addr ${fdtaddr}
fdt resize ${fdt_buffer}
fdt apply ${rdaddr}
fdt resize ${fdt_buffer}

load mmc ${bootpart} ${rdaddr} /lib/firmware/BB-BONE-eMMC1-01-00A0.dtbo
fdt addr ${fdtaddr}
fdt resize ${fdt_buffer}
fdt apply ${rdaddr}
fdt resize ${fdt_buffer}

load mmc ${bootpart} ${rdaddr} /lib/firmware/BB-HDMI-TDA998x-00A0.dtbo
fdt addr ${fdtaddr}
fdt resize ${fdt_buffer}
fdt apply ${rdaddr}
fdt resize ${fdt_buffer}

setenv bootargs console=ttyS0,115200n8 root=PARTUUID=${uuid} rw rootfstype=ext4 rootwait coherent_pool=1M net.ifnames=0 lpj=1990656 rng_core.default_quality=100 init=/lib/systemd/systemd
load mmc ${bootpart} ${loadaddr} /boot/vmlinuz-${uname}
load mmc ${bootpart} ${rdaddr} /boot/initrd.img-${uname}
bootz ${loadaddr} ${rdaddr}:${filesize} ${fdtaddr}

