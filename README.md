board-beagleboard
=================

This source package provides default "device" and "board"
meta-packages for all supported members of the Beagleboard target
family. I lumped them all together for convenience, and because
they're all nearly identical.

A consequence of the above is that Pragmatux now owns the
"device-[board name]" package namespace, i.e. when you want to
customize your own device setup then you'll have to use a different
name.
